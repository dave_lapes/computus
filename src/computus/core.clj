(ns computus.core
  (:import (java.time LocalDate)))

(def repetitive-holidays-dates-maps
  [{:month 1 :day 1}
   {:month 5 :day 1}
   {:month 5 :day 8}
   {:month 5 :day 21}
   {:month 5 :day 22}
   {:month 5 :day 31}
   {:month 6 :day 1}
   {:month 6 :day 5}
   {:month 12 :day 24}
   {:month 12 :day 25}
   {:month 12 :day 26}
   {:month 12 :day 31}])

(def easter-holidays-offsets
  [-7                                                       ; Sunday before Easter
   -3                                                       ; Green Thursday
   -2                                                       ; Big Friday
   -1                                                       ; White Saturday
   0                                                        ; Easter Sunday
   1                                                        ; Resurrection Monday
   ])

(defn easter-date
  "Returns LocalDate instance representing date of the Easter holiday, specifically the Sunday."
  [year]
  (let [year (int year)
        a (mod year 19)
        b (mod year 4)
        c (mod year 7)
        k (-> (/ year 100)
              Math/floor
              int)
        p (-> (/ (+ 13 (* 8 k)) 25)
              Math/floor
              int)
        q (-> (/ k 4)
              Math/floor
              int)
        M (mod (-> (- 15 p) (+ k) (- q)) 30)
        N (mod (-> (+ 4 k) (- q)) 7)
        d (mod (-> (* 19 a) (+ M)) 30)
        e (mod (-> (* 2 b) (+ (* 4 c)) (+ (* 6 d)) (+ N)) 7)
        ^int easter-march-day (+ 22 d e)
        ^int easter-april-day (- (+ d e) 9)]
    (if (> easter-march-day 31)
      (if (and (= d 29) (= e 6))
        (LocalDate/of year 4 19)
        (if (and (= d 28) (= e 6) (< (mod (+ (* 11 M) 11) 30) 19))
          (LocalDate/of year 4 18)
          (LocalDate/of year 4 easter-april-day)))
      (LocalDate/of year 3 easter-march-day))))

(defn easter-holidays-dates
  "Returns vector of LocalDate instances of all days of Easter holidays for given year"
  [year]
  (let [easter-date (easter-date year)]
    (map #(.plusDays easter-date %)  easter-holidays-offsets)))

(defn repetitive-holidays-dates
  "Returns vector of LocalDate instances of all repetitive holidays which don't depend on year,
   but happen on same date every year."
  [year]
  (let [year (int year)]
    (map #(LocalDate/of year (:month %) (:day %)) repetitive-holidays-dates-maps)))

(defn holidays-dates
  "Returns vector of LocalDate instances of all holidays for given year"
  [year]
  (concat (easter-holidays-dates year) (repetitive-holidays-dates year)))

(defn is-a-holiday?
  "Returns true if given day is a holiday, otherwise returns false."
  [^LocalDate date]
  (boolean (some #(.equals date %) (holidays-dates (.getYear date)))))

(defn is-a-weekend?
  "Returns true if given day is a weekend, otherwise returns false."
  [^LocalDate date]
  (let [day-of-week (.getValue (.getDayOfWeek date))]
    (or (= day-of-week 6)
        (= day-of-week 7))))

(defn is-a-workday?
  "Returns true if given day is nor a weekend or a holiday, otherwise returns false."
  [^LocalDate date]
  (not (or (is-a-weekend? date)
           (is-a-holiday? date))))

(defn find-first-workday
  "Returns first available workday as LocalDate. If given date is already a workday, it gets returned.;"
  [^LocalDate date]
  (if (is-a-workday? date)
    date
    (find-first-workday (.plusDays date 1))))
