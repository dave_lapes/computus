(ns computus.core-test
  (:require [clojure.test :refer :all]
            [computus.core :refer :all])
  (:import (java.time LocalDate)))

(deftest repetitive-holidays-dates-test
  (is (= #{{:month 1 :day 1}
           {:month 5 :day 1}
           {:month 5 :day 8}
           {:month 5 :day 21}
           {:month 5 :day 22}
           {:month 5 :day 31}
           {:month 6 :day 1}
           {:month 6 :day 5}
           {:month 12 :day 24}
           {:month 12 :day 25}
           {:month 12 :day 26}
           {:month 12 :day 31}}
         (set repetitive-holidays-dates-maps))))

(deftest easter-offsets-test
  (is (= #{-7 -3 -2 -1 0 1} (set easter-holidays-offsets))))

(deftest easter-date-test
  (are [x y] (.equals x y)
             (LocalDate/of 2000 04 23) (easter-date 2000)
             (LocalDate/of 2001 04 15) (easter-date 2001)
             (LocalDate/of 2002 03 31) (easter-date 2002)
             (LocalDate/of 2003 04 20) (easter-date 2003)
             (LocalDate/of 2004 04 11) (easter-date 2004)
             (LocalDate/of 2005 03 27) (easter-date 2005)
             (LocalDate/of 2006 04 16) (easter-date 2006)
             (LocalDate/of 2007 04 8) (easter-date 2007)
             (LocalDate/of 2008 03 23) (easter-date 2008)
             (LocalDate/of 2009 04 12) (easter-date 2009)
             (LocalDate/of 2010 04 4) (easter-date 2010)
             (LocalDate/of 2011 04 24) (easter-date 2011)
             (LocalDate/of 2012 04 8) (easter-date 2012)
             (LocalDate/of 2013 03 31) (easter-date 2013)
             (LocalDate/of 2014 04 20) (easter-date 2014)
             (LocalDate/of 2015 04 5) (easter-date 2015)
             (LocalDate/of 2016 03 27) (easter-date 2016)
             (LocalDate/of 2017 04 16) (easter-date 2017)
             (LocalDate/of 2018 04 1) (easter-date 2018)
             (LocalDate/of 2019 04 21) (easter-date 2019)
             (LocalDate/of 2020 04 12) (easter-date 2020)
             (LocalDate/of 2021 04 4) (easter-date 2021)
             (LocalDate/of 2022 04 17) (easter-date 2022)
             (LocalDate/of 2023 04 9) (easter-date 2023)
             (LocalDate/of 2024 03 31) (easter-date 2024)
             (LocalDate/of 2025 04 20) (easter-date 2025)
             (LocalDate/of 2026 04 5) (easter-date 2026)
             (LocalDate/of 2027 03 28) (easter-date 2027)
             (LocalDate/of 2028 04 16) (easter-date 2028)
             (LocalDate/of 2029 04 1) (easter-date 2029)
             (LocalDate/of 2030 04 21) (easter-date 2030)
             (LocalDate/of 2031 04 13) (easter-date 2031)
             (LocalDate/of 2032 03 28) (easter-date 2032)
             (LocalDate/of 2033 04 17) (easter-date 2033)
             (LocalDate/of 2034 04 9) (easter-date 2034)
             (LocalDate/of 2035 03 25) (easter-date 2035)
             (LocalDate/of 2036 04 13) (easter-date 2036)
             (LocalDate/of 2037 04 5) (easter-date 2037)
             (LocalDate/of 2038 04 25) (easter-date 2038)
             (LocalDate/of 2039 04 10) (easter-date 2039)
             (LocalDate/of 2040 04 1) (easter-date 2040)))

(deftest easter-holidays-dates-test
  (is (let [year 2020]
        (= #{(LocalDate/of year 4 5)
             (LocalDate/of year 4 9)
             (LocalDate/of year 4 10)
             (LocalDate/of year 4 11)
             (LocalDate/of year 4 12)
             (LocalDate/of year 4 13)}
           (set (easter-holidays-dates year))))))

(deftest repetitive-holidays-dates-test
  (is (let [year 2020]
        (= #{(LocalDate/of year 1 1)
                 (LocalDate/of year 5 1)
                 (LocalDate/of year 5 8)
                 (LocalDate/of year 5 21)
                 (LocalDate/of year 5 22)
                 (LocalDate/of year 5 31)
                 (LocalDate/of year 6 1)
                 (LocalDate/of year 6 5)
                 (LocalDate/of year 12 24)
                 (LocalDate/of year 12 25)
                 (LocalDate/of year 12 26)
                 (LocalDate/of year 12 31)}
           (set (repetitive-holidays-dates year))))))

(deftest holidays-dates-test
  (is (let [year 2020]
        (= #{(LocalDate/of year 4 5)
             (LocalDate/of year 4 9)
             (LocalDate/of year 4 10)
             (LocalDate/of year 4 11)
             (LocalDate/of year 4 12)
             (LocalDate/of year 4 13)
             (LocalDate/of year 1 1)
             (LocalDate/of year 5 1)
             (LocalDate/of year 5 8)
             (LocalDate/of year 5 21)
             (LocalDate/of year 5 22)
             (LocalDate/of year 5 31)
             (LocalDate/of year 6 1)
             (LocalDate/of year 6 5)
             (LocalDate/of year 12 24)
             (LocalDate/of year 12 25)
             (LocalDate/of year 12 26)
             (LocalDate/of year 12 31)}
           (set (concat (repetitive-holidays-dates year) (easter-holidays-dates year)))))))

(deftest is-a-holiday-test
  (is (is-a-holiday? (LocalDate/of 2020 4 13))))

(deftest is-not-a-holiday-test
  (is (not (is-a-holiday? (LocalDate/of 2020 4 14)))))

(deftest is-a-weekend-test
  (is (is-a-weekend? (LocalDate/of 2020 4 12))))

(deftest is-not-a-weekend-test
  (is (not (is-a-weekend? (LocalDate/of 2020 4 13)))))

(deftest is-a-workday-test
  (are [x] (is-a-workday? x)
           (LocalDate/of 2020 4 6)
           (LocalDate/of 2020 4 7)
           (LocalDate/of 2020 4 8)))

(deftest is-not-a-workday-test
  (are [x] (not (is-a-workday? x))
           (LocalDate/of 2020 4 5)
           (LocalDate/of 2020 4 9)
           (LocalDate/of 2020 4 10)
           (LocalDate/of 2020 4 11)
           (LocalDate/of 2020 4 12)
           (LocalDate/of 2020 4 13)
           (LocalDate/of 2020 4 18)
           (LocalDate/of 2020 4 19)))

(deftest find-first-workday-test
  (are [x y] (.equals (find-first-workday x) y)
             (LocalDate/of 2020 4 4) (LocalDate/of 2020 4 6)
             (LocalDate/of 2020 4 5) (LocalDate/of 2020 4 6)
             (LocalDate/of 2020 4 7) (LocalDate/of 2020 4 7)
             (LocalDate/of 2020 4 9) (LocalDate/of 2020 4 14)
             (LocalDate/of 2020 4 10) (LocalDate/of 2020 4 14)
             (LocalDate/of 2020 4 11) (LocalDate/of 2020 4 14)
             (LocalDate/of 2020 4 12) (LocalDate/of 2020 4 14)
             (LocalDate/of 2020 4 13) (LocalDate/of 2020 4 14)
             (LocalDate/of 2020 4 14) (LocalDate/of 2020 4 14)))
